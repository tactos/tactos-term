#include <string>
#include <X11/Xaw/Box.h>

#include "box.h"


namespace ncurses
{
    
    /**
    * @brief set a title to to the box
    * 
    * @param tit title to set
    */
    void Box::setTitle(std::string tit )
    {
        this->title = tit;
            if ( tit.size()> this->columns-2)
            {
                this->columns = tit.size()+2;
                this->refresh_box();
            }
        int pos_title = (this->columns - tit.size())/2 ;
        mvwhline(this->local_box, 2,1,'-', columns-2);
        mvwhline(this->local_box, 1,1,' ', columns-2); // remove previous drawing
        mvwprintw(this->local_box, 1, pos_title, tit.c_str());
        wrefresh(this->local_box);
    }

    /**
    * @brief force refresshing all box 
    * 
    */
    void Box::refresh_box()
    {
        wborder(this->local_box, ' ', ' ', ' ',' ',' ',' ',' ',' ');
        wresize(this->local_box, this->lines, this->columns);
        box(this->local_box, '|', '-');
        wrefresh(this->local_box);
    }

    /**
    * @brief change size of the box. the size contains the border
    * 
    * @param lin number of lines
    * @param col number of column
    */
    void Box::resize_box(int lin, int col)
    {
        this->columns = col;
        this->lines = lin;
        this->refresh_box();
        this->setTitle(this->title);
        wrefresh(this->local_box);
    }

    /**
    * @brief write data inside the box
    * 
    * @param x initial x position of text
    * @param y initial y position of text
    * @param msg text to write
    */
    void Box::write(int x, int y, std::string msg)
    {
        if (!this->title.empty()) {
            x += 2; // ignore title in window
        }
        mvwprintw(this->local_box, x, y, msg.c_str());
        wrefresh(this->local_box);
    }



    /**
    * @brief base box constructor. create a new window with border
    * 
    * @param lines number of lines for the box
    * @param column number of columns for the box
    * @param x x position of the left corner of the box
    * @param y y position of the left corner of the box
    */
    Box::Box(int lines, int column, int x, int y):
    lines(lines), columns(column)
    {
        this->local_box = newwin(lines, column, x, y);
        refresh();
        box(this->local_box, '|', '-');
        wrefresh(this->local_box );
    }

    /**
    * @brief delete box
    * 
    */
    Box::~Box()
    {
        wborder(this->local_box, ' ', ' ', ' ',' ',' ',' ',' ',' ');
        for (uint i = 1 ; i < this->lines ; ++i)
        {
            mvwhline(this->local_box, i,1,' ', columns-2);
        }
        wrefresh(this->local_box );
        delwin(this->local_box);
    }
    
}
