#include <ncurses.h>

#include"ui.h"

namespace ncurses {
    
    /**
    * @brief initialise ncurses drawing
    * 
    */
    UI::UI()
    {
        initscr();
        noecho();
        curs_set(FALSE);
    }

    UI::~UI()
    {
        endwin();
    }
    
        /**
    * @brief create a box of size lines*column with a border of 1 inside. save the box with the name "name"
    * 
    * @param lines : number of line for the box
    * @param column number of column of the box
    * @param x x position
    * @param y y position
    * @param tit title
    */
    Box* UI::create_box(int lines, int column, int x, int y, std::string tit)
    {
        Box* box = new Box(lines, column, x, y);
        this->boxes.push_back(box);
        if (!tit.empty()) {
            this->boxes.back()->setTitle(tit);
            
        }
        return this->boxes.back();
    }
}
