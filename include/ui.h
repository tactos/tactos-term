#ifndef UI_H
#define UI_H

#include <vector>
#include <string>

#include "box.h"

namespace ncurses
{
        class UI {
    public:
        UI();
        Box* create_box(int lines, int column, int x, int y, std::string tit);
        ~UI();
    private:
        std::vector<Box*> boxes;
    };
    
}
#endif //UI_H
