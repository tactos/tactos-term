#ifndef BOX_H
#define BOX_H

#include<memory>
#include <ncurses.h>

namespace ncurses
{
    class Box {
    public:
        Box(int lines, int column, int x, int y);
        void update_box(int x, int y, std::string text);
        void setTitle(std::string title);
        int remove_box();
        void resize_box(int lin, int col);
        void write(int x, int y, std::string msg);
        ~Box();
        
    private:
        WINDOW* local_box;
        void refresh_box();
        std::string title;
        unsigned int lines;
        unsigned int columns;
    };
}

#endif //BOX_H
