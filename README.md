# Tactos-term

![tactos-term screenshot](doc/imgs/tactos-term.png)

Tactos-term is an interface mainly used for debug purpose. This interface display the matrix read from dbus interface segment managed by [tactos-core-color](https://gitlab.com/tactos/tactos-core-color) in the terminal thanks to [ncurses](https://en.wikipedia.org/wiki/Ncurses) library.

## Building

### Dependencies

- meson (build system)
- ncurses
- giomm (dbus service dependency)

### compilation

```bash
meson --buildtype=release builddir
cd builddir
ninja
./main_ncurses
```

### Documentation

A doxygen documentation can be found [here](https://tactos.gitlab.io/tactos-term/)


## Contact

This project is developed by the the laboratory of [Costech](http://www.costech.utc.fr/) in the [University of Technology of Compiegne (UTC)](https://www.utc.fr/) in partnership with [hypra](http://hypra.fr/-Accueil-1-.html) and [natbraille](http://natbraille.free.fr/).
