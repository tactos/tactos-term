// #include "unistd"
#include "tactosIPC_proxy.h"
#include <ui.h>

#include<functional>

typedef std::shared_ptr<ncurses::Box>  box_ptr;

ncurses::UI ui;
box_ptr tactos;
bool publisher;
Glib::RefPtr<org::tactos::ipcProxy> proxy;

void grid_cbk(box_ptr b, std::vector<bool> values)
{
    int i = 0;
    for (bool val : values ) 
    {
        std::string disp = val?"X":" ";
        b->write(i/4+1, i%4+1, disp);
        i++;
    }
}

void on_matrix_updated()
{
    bool valid = true;
    std::vector<bool> mat = proxy->Matrix_get(&valid);
    if(valid) {
        grid_cbk(tactos, mat);
    }
}


void drawUI(Glib::RefPtr<org::tactos::ipcProxy> proxy)
{
    tactos.reset();
    publisher = proxy->Publisher_get();
    if (publisher)
    {
        tactos = box_ptr(ui.create_box(6,6, 1, 1, ""));
    } else {
        std::string str = "no publisher enabled";
        tactos = box_ptr(ui.create_box(3, str.length()+2, 1, 10, ""));
        tactos->write(1,1,str);
    }
}

void signalHandler( int signum ) {
    std::cout << "Interrupt signal (" << signum << ") received.\n";
    
    // cleanup and close up stuff here  
    // terminate program  
    proxy->readerLeave_sync();
    exit(signum);  
}


int main() {
    Glib::init();
    Gio::init();
    
    signal(SIGINT, signalHandler);  
    ui = ncurses::UI();
    
    proxy = org::tactos::ipcProxy::createForBus_sync(Gio::DBus::BUS_TYPE_SESSION,
                                     Gio::DBus::PROXY_FLAGS_NONE,
                                     "org.tactos.ipc",
                                     "/org/tactos/ipc",
                                     Gio::Cancellable::create());
    
    proxy->Matrix_changed().connect(&on_matrix_updated);
    proxy->readerJoin_sync();
    drawUI(proxy);
    proxy->Publisher_changed().connect(std::bind(drawUI, proxy));
    
    Glib::RefPtr<Glib::MainLoop> ml = Glib::MainLoop::create();
    ml->run();
    
    return 0;
}
